var resources = {
    "ch": {
        "translation": {
            "title": "页面标题",
            "placeholder_1": "输入您的名字",
            "value_1": "值",
			"TAC_title": "條款及細則",
			"TAC": "TEST",
		}
    },
        "en": {
        "translation": {
            "title": "title of the page",
            "placeholder_1": "enter name",
            "value_1": "value 1",
			"TAC_title": "",
			"TAC": "TEST",
        }
    }
};

$(document).ready(function () {
    i18n.init({
        "lng": 'en',
        "resStore": resources,
        "fallbackLng" : 'en'
    }, function (t) {
        $(document).i18n();
    });

    $('.lang').click(function () {
        var lang = $(this).attr('data-lang');
        i18n.init({
            lng: lang
        }, function (t) {
            $(document).i18n();
        });
    });
});
