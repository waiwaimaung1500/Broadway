function showMenu_old() {
	
	// FOR DEBUG USE
	//////////////////////////////
	login = false;
	//////////////////////////////
    overlayElement = document.createElement("div");
    overlayElement.className = 'modalOverlay';
    modalWindowElement = document.createElement("div");
    modalWindowElement.className = 'modalWindow';
	/* modalWindowElement.style.cssFloat = "left"; */
	/* modalWindowIcon = document.createElement("img");
	   modalWindowIcon.className = 'modalWindowIcon';
	   modalWindowIcon.src = "img/icon.png";
	   modalWindowIcon.src = "img/" + icon;
	   modalWindowIcon.style.cssFloat = "left";
	   modalWindowIcon.height = "40";
	   modalWindowIcon.width = "40"; */
	
	modalWindowTitle = document.createTextNode("TEST");
	modalWindowTitle.className = 'modalWindowTitle';
	modalWindowMsg = document.createTextNode("MESSAGE");
	modalWindowMsg = document.createElement("div");
	modalWindowMsg.className = 'modalWindowMsg';
    modalWindowMsg.innerHTML = "INNER";
	console.log("TEST");
	var template =
	"<img id='optioncolourbtn' src='img/option-colour.png' >" +
	"<div id='optionitems'>" +
	"<form id='option'>" +
		"<span class='option' data-l10n-id='language'>語言</span>" +
		"<div id='language'><input type='radio' name='language' value='zh-hant' checked='true'><span class='option' data-l10n-id='traditional' >繁體</span>" +
		"<input type='radio' name='language' value='zh-cn'><span class='option' data-l10n-id='simplified' >簡體</span></div>" +
		"</form>" +
		
		//Part for the link for website and login/logout
		"<a href='https://www.broadway.com.hk' data-inline='true'>" +
		"<div class='option' data-l10n-id='website'>百老滙網頁</div>" +
		"</a>";
	if(login) {
		template = template +
		"<a href='' data-inline='true'>" +
		"<div class='option' data-l10n-id='logout'>登出</div>" +
		"</a>" +
		"</div>";
	} 	
	// Setting for width and height
	if(navigator.userAgent.match(/Android\s4\.[0123]/)){
		modalWindowElement.style.width = "60%";
	} else {
		modalWindowElement.style.width = "60vw";
	}
	if(navigator.userAgent.match(/Android\s4\.[0123]/)){
		modalWindowElement.style.marginTop = "1%";
	} else {
		/* modalWindowElement.style.marginTop = "1vh"; */
	}
	
	$('.ui-page-active').children().append(overlayElement);
	$('.ui-page-active').children().append(modalWindowElement);
    /* document.body.appendChild(overlayElement);
       document.body.appendChild(modalWindowElement); */
	/* modalWindowElement.appendChild(modalWindowIcon); */
	/* modalWindowElement.appendChild(modalWindowTitle); */
	/* modalWindowElement.appendChild(modalWindowMsg); */
	/* $('modalWindow').children().append(template); */
	$('.modalWindow').append(template);
	/* $('.modalWindow').append(link); */
	/* modalWindowElement.appendChild(template); */
	/* $(modalWindowElement).append($'<img'>, {
	   src: "img/icon.png",
	   })); */
    setTimeout(function() {
        modalWindowElement.style.opacity = 1;
        overlayElement.style.opacity = 0;
        overlayElement.addEventListener("click", hideMenu, false);
		/* setTimeout(hideMenu, 10000); */
    }, 100);

	$('input[name=language]').on("click", {
		lang: $('input[name=language]:checked').val()
	}, changeLanguage );
	
	/* $('input[name=language]').on("click", function() {
	   console.log("T");
	   }); */
}

//hide the modal overlay and popup window
function hideMenu() {
    modalWindowElement.style.opacity = 0;
    overlayElement.style.opacity = 0;
    overlayElement.removeEventListener("click", hideMenu, false);
    setTimeout(function() {
		$('.modalOverlay').remove();
		$('.modalWindow').remove();
        /* document.body.removeChild(overlayElement);
           document.body.removeChild(modalWindowElement); */
    }, 100);
}
