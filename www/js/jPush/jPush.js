var jPush = {
    'notificationId': 0,
    'messageObj': null,
    'notification': null,

    "init": function() {
        try {
            window.plugins.jPushPlugin.init();
            getRegistrationID();
            if (device.platform != "Android") {
                window.plugins.jPushPlugin.setDebugModeFromIos();
                window.plugins.jPushPlugin.setApplicationIconBadgeNumber(0);
            } else {
                window.plugins.jPushPlugin.setDebugMode(false);
                //window.plugins.jPushPlugin.setStatisticsOpen(false);
            }
        } catch (exception) {
            console.log(exception);
        }
    },

    "onReceiveNotification": function(event) {
        try {
            var alertContent;
            if (device.platform == "Android") {
                jPush.notification = window.plugins.jPushPlugin.receiveNotification;
                alertContent = window.plugins.jPushPlugin.receiveNotification.alert;
                pageOpen = jPush.notification.extras.pageOpen;
            } else {
                jPush.notification = event.aps;
                alertContent = event.aps.alert;
            }
        } catch (exception) {
            console.log(exception);
        }
    },

    "onOpenNotification": function (event) {
        try {
            var alertContent;
            if (device.platform == "Android") {
                jPush.notification = window.plugins.jPushPlugin.openNotification;
                alertContent = window.plugins.jPushPlugin.openNotification.alert;
                pageOpen = jPush.notification.extras.pageOpen;
            } else {
                jPush.notification = event.aps;
                alertContent = event.aps.alert;
            }
        } catch (exception) {
            console.log("JPushPlugin:onOpenNotification" + exception);
        }  
    },

    "onReceiveMessage": function (event) {
        try {
            var message;
            if (device.platform == "Android") {
                jPush.messageObj = window.plugins.jPushPlugin.receiveMessage;
			          // cordova.plugins.notification.local.schedule({
			          //     id: ++jPush.notificationId,
			          //     title: jPush.messageObj.extras.title == null ? "Broadway" : jPush.messageObj.extras.title,
			          //     text: jPush.messageObj.message,
			          //     icon: "res://icon.png",
			          //     smallIcon: "res://icon.png"
		            // });
            } else {
                jPush.messageObj = event.content;
            }
            $("#messageResult").html(message);
        } catch (exception) {
            console.log("JPushPlugin:onReceiveMessage-->" + exception);
        }
    },
}
