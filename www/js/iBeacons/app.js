function broadwayiBeaconSetting() {
	
	broadwayiBeacon = (function()
		{
			// Application object.
				var broadwayiBeacon = {};

			// History of enter/exit events.
				var mRegionEvents = [];

			// Nearest ranged beacon.
				var mNearestBeacon = null;

			// Timer that displays nearby beacons.
				var mNearestBeaconDisplayTimer = null;

			// Background flag.
				var mAppInBackground = false;

			// Background notification id counter.
				var mNotificationId = 0;

			// Mapping of region event state names.
				// These are used in the event display string.
				var mRegionStateNames =
			{
				'CLRegionStateInside': 'Enter',
				'CLRegionStateOutside': 'Exit'
			};

			// Here monitored regions are defined.
				// TODO: Update with uuid/major/minor for your beacons.
				// You can add as many beacons as you want to use.
				var mRegions =
			[
				{
					id: 'region1',
					uuid: 'fda50693-a4e2-4fb1-afcf-c6eb07647825',
					major: 20,
					minor: 31686
				},
				{
					id: 'region2',
					uuid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
					major: 60378,
					minor: 22122
				}
			];

			mRegions = g_mRegions;

			//console.log(mRegions[0].uuid);
			// Region data is defined here. Mapping used is from
			// region id to a string. You can adapt this to your
			// own needs, and add other data to be displayed.
				// TODO: Update with major/minor for your own beacons.
				var mRegionData =
			{
				'region1': 'Region One',
				'region2': 'Region Two'
			};

			mRegionData = g_mRegionData;
			console.log(mRegionData.region1);

			broadwayiBeacon.initialize = function()
			{
				console.log('iBeacon initialize');
				document.addEventListener('deviceready', onDeviceReady, false);
				document.addEventListener('pause', onAppToBackground, false);
				document.addEventListener('resume', onAppToForeground, false);
			};

			function onDeviceReady()
			{
				startMonitoringAndRanging();
				startNearestBeaconDisplayTimer();
			}

			function onAppToBackground()
			{
				mAppInBackground = true;
				stopNearestBeaconDisplayTimer();
			}

			function onAppToForeground()
			{
				mAppInBackground = false;
				startNearestBeaconDisplayTimer();
			}

			function startNearestBeaconDisplayTimer()
			{
				mNearestBeaconDisplayTimer = setInterval(displayNearestBeacon, 1000);
			}

			function stopNearestBeaconDisplayTimer()
			{
				clearInterval(mNearestBeaconDisplayTimer);
				mNearestBeaconDisplayTimer = null;
			}

			function startMonitoringAndRanging()
			{
				function onDidDetermineStateForRegion(result)
				{
					saveRegionEvent(result.state, result.region.identifier);
					displayRecentRegionEvent();
				}

				function onDidRangeBeaconsInRegion(result)
				{
					updateNearestBeacon(result.beacons);
				}

				function onError(errorMessage)
				{
					console.log('Monitoring beacons did fail: ' + errorMessage);
				}

				// Request permission from user to access location info.
					cordova.plugins.locationManager.requestAlwaysAuthorization();

				// Create delegate object that holds beacon callback functions.
					var delegate = new cordova.plugins.locationManager.Delegate();
				cordova.plugins.locationManager.setDelegate(delegate);

				// Set delegate functions.
					delegate.didDetermineStateForRegion = onDidDetermineStateForRegion;
				delegate.didRangeBeaconsInRegion = onDidRangeBeaconsInRegion;

				// Start monitoring and ranging beacons.
					startMonitoringAndRangingRegions(mRegions, onError);
			}

			function startMonitoringAndRangingRegions(regions, errorCallback)
			{
				// Start monitoring and ranging regions.
					for (var i in regions)
				{
					startMonitoringAndRangingRegion(regions[i], errorCallback);
				}
			}

			function startMonitoringAndRangingRegion(region, errorCallback)
			{
				// Create a region object.
					var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
						region.id,
						region.uuid
					);
				console.log(region);
				console.log("beaconRegion: " + beaconRegion);

				// Start ranging.
					cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
					.fail(errorCallback)
					.done();

				// Start monitoring.
					cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion)
					.fail(errorCallback)
					.done();
			}

			function saveRegionEvent(eventType, regionId)
			{
				// Save event.
					mRegionEvents.push(
						{
							type: eventType,
							time: getTimeNow(),
							regionId: regionId
						});

				// Truncate if more than ten entries.
					if (mRegionEvents.length > 5)
				{
					mRegionEvents.shift();
				}
			}

			function getBeaconId(beacon)
			{
				return beacon.uuid + ':' + beacon.major + ':' + beacon.minor;
			}

			function isSameBeacon(beacon1, beacon2)
			{
				return getBeaconId(beacon1) == getBeaconId(beacon2);
			}

			function isNearerThan(beacon1, beacon2)
			{
				return beacon1.accuracy > 0
				&& beacon2.accuracy > 0
				&& beacon1.accuracy < beacon2.accuracy;
			}

			function updateNearestBeacon(beacons)
			{
				for (var i = 0; i < beacons.length; ++i)
				{
					var beacon = beacons[i];
					if (!mNearestBeacon)
					{
						mNearestBeacon = beacon;
					}
					else
					{
						if (isSameBeacon(beacon, mNearestBeacon) ||
							isNearerThan(beacon, mNearestBeacon))
						{
							mNearestBeacon = beacon;
						}
					}
				}
			}

			function displayNearestBeacon()
			{
				if (!mNearestBeacon) { return; }

				// Clear element.
					$('#beacon').empty();

				// Update element.
					var element = $(
						'<li>'
							+	'<strong>Nearest Beacon</strong><br />'
							+	'UUID: ' + mNearestBeacon.uuid + '<br />'
							+	'Major: ' + mNearestBeacon.major + '<br />'
							+	'Minor: ' + mNearestBeacon.minor + '<br />'
							+	'Proximity: ' + mNearestBeacon.proximity + '<br />'
							+	'Distance: ' + mNearestBeacon.accuracy + '<br />'
							+	'RSSI: ' + mNearestBeacon.rssi + '<br />'
							+ '</li>'
					);
				$('#beacon').append(element);
			}

			function displayRecentRegionEvent()
			{
				console.log('displayRecentRegionEvent ' + mAppInBackground);
				if (mAppInBackground)
				{
					console.log('background');
					// Set notification title.
						var event = mRegionEvents[mRegionEvents.length - 1];
					if (!event) { return; }
					var title = getEventDisplayString(event);
					
					// Check repeated iBeacon
					var loopend = false;
					for(var i = 0; i < iBeaconTempMessage.length; i++) {
						if(!loopend) {
							console.log(title);
							console.log(iBeaconTempMessage[i]);
							if(title == iBeaconTempMessage[i]) {
								console.log('true');
								repeat = true;
								loopend = true;
							} else {
								repeat = false;
							}
						}
					}
					
					if(iBeaconTempMessage.length == 0) {
						iBeaconTempMessage[0] = title;
					}
					if(!loopend) {
						iBeaconTempMessage[iBeaconTempMessage.length] = title;
					}
					
					
					// Create notification.
						if(mRegionStateNames[event.type] == "Enter") {
							if(!repeat) {
								cordova.plugins.notification.local.schedule({
									id: ++mNotificationId,
									title: "BROADWAY",
									text: title,
									icon: "res://icon.png",
									smallIcon: "res://icon.png"
								});
							} else {
								/*
								cordova.plugins.notification.local.schedule({
									id: ++mNotificationId,
									title: "TESTUSE",
									text: "LAST RECEIVED: " + iBeaconTempMessage[iBeaconTempMessage.length - 1],
									icon: "res://icon.png",
									smallIcon: "res://icon.png"
								});
								*/
							}
						}
				}
				else
				{
					//displayRegionEvents();
				}
			}

			function getEventDisplayString(event)
			{
				/* if(mRegionStateNames[event.type].includes("Enter")) {
				   return event.time + ': '
				   + mRegionStateNames[event.type] + ' '
				   + mRegionData[event.regionId];
				   
				   } */
				/* return event.type + ': '
				   + mRegionStateNames[event.type] + ' '
				   + mRegionData[event.regionId]; */
				return mRegionData[event.regionId];
			}

			function getTimeNow()
			{
				function pad(n)
				{
					return (n < 10) ? '0' + n : n;
				}

				function format(h, m, s)
				{
					return pad(h) + ':' + pad(m)  + ':' + pad(s);
				}

				var d = new Date();
				return format(d.getHours(), d.getMinutes(), d.getSeconds());
			}

			return broadwayiBeacon;

		})();

}
