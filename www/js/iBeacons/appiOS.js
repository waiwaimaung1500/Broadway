/* Notifcation generated method changed to when the application 
 * in the foreground it will check the iBeacon signal, when the app 
 * backs to background, the notifcation is generated. The generation 
 * of the notification is handled by js/app.js
 */
var BroadwayiBeacon = {
    "notificationId": 0,
    "test": true,
    "regions": null,
    "regionData": null,
    "regionURL": null,
    "beaconRegion": null,
    "repeatRegion" :null,
    "repeatTime": null,
    "delegate": null,
    "latestMessage": null,
    "latestURL": null,
    "latestId": 0,
    "repeat": false,
    "TIMEBOUND": 1800000,

    "init": function() {
	this.regions = g_mRegions;
	this.regionData = g_mRegionData;
	this.regionURL = g_mRegionURL;
	if(this.test) {
	    BroadwayiBeacon.regions.push(
		{
		    id: "Test1",
		    major: "20",
		    minor: "31684",
		    uuid: "fda50693-a4e2-4fb1-afcf-c6eb07647825"
		},
		{
		    id: "Test2",
		    major: "20",
		    minor: "31686",
		    uuid: "fda50693-a4e2-4fb1-afcf-c6eb07647825"
		}
	    );
	    BroadwayiBeacon.regionData.Test1 = "Test1";
	    BroadwayiBeacon.regionData.Test2 = "Test2";
	    // Case for the database without region URL
	    if(typeof(BroadwayiBeacon.regionURL) == 'undefined') {
	    	BroadwayiBeacon.regionURL = new Object();
	    	BroadwayiBeacon.regionURL.Test1 = "member-promotions";
	    	BroadwayiBeacon.regionURL.Test2 = "hotproducts";
	    }
	    BroadwayiBeacon.repeatRegion = [];
	}

	this.initBeaconRegion(this.regions[0]);
	// cordova.plugins.locationManager.getMonitoredRegions();

	// Get back BroadwayiBeacon.repeatTime

	window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
			dir.getFile("timelog.txt", {create:false}, function(fileEntry) {
				BroadwayiBeacon.readFile(fileEntry);
			});
		});
	this.startMonitoring();
    },

    "writeFile": function(fileEntry, dataObj) {
    	// Create a FileWriter object for our FileEntry (log.txt).
	    fileEntry.createWriter(function (fileWriter) {

	        fileWriter.onwriteend = function() {
	            console.log("Successful file read...");
	            BroadwayiBeacon.readFile(fileEntry);
	        };

	        fileWriter.onerror = function (e) {
	            console.log("Failed file read: " + e.toString());
	        };

	        // If data object is not passed in,
	        // create a new Blob instead.
	        if (!dataObj) {
	            dataObj = new Blob([new Date().getTime(), ',', BroadwayiBeacon.repeatRegion], { type: 'text/plain' });
	        } else {
		    // Case for time is not required for update
	            dataObj = new Blob([dataObj, ',', BroadwayiBeacon.repeatRegion], { type: 'text/plain' });
		}

	        fileWriter.write(dataObj);
	    });
    },

    "readFile": function(fileEntry) {
    	fileEntry.file(function (file) {
	        var reader = new FileReader();

	        reader.onloadend = function() {
	            console.log("Successful file read: " + this.result);
		    result = this.result.split(',');
	            
	            // Get back the time 
	            BroadwayiBeacon.repeatTime = result[0];
		    if(BroadwayiBeacon.repeatRegion.length == 0) {
			for(i = 1; i < result.length; i++)
			    BroadwayiBeacon.repeatRegion.push(result[i]);
		    }
	        };

	        reader.readAsText(file);

	    }, console.log('read Error'));
    },

    "initBeaconRegion": function(region) {
		this.beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
			"BroadwayiBeacon",
			region.uuid,
			undefined,
			undefined
		);
		this.beaconRegion.notifyEntryStateOnDisplay = true;
    },

    "startMonitoring": function() {
	function onError(errorMessage) {
	    console.log('Monitoring beacons did fail: ' + errorMessage);
	}

	function startRanging() {
	    cordova.plugins.locationManager.startRangingBeaconsInRegion(BroadwayiBeacon.beaconRegion)
		.fail(function(e) { console.error(e); })
		.done();
	}

	function stopRanging(beacon) {
	    cordova.plugins.locationManager.stopRangingBeaconsInRegion(BroadwayiBeacon.beaconRegion)
		.fail(onError)
		.done();
	    cordova.plugins.locationManager.startMonitoringForRegion(BroadwayiBeacon.beaconRegion)
		.fail(onError)
		.done();
	    BroadwayiBeacon.repeat = false;
	}

	function notificationGeneration(message, page_url) {
		if(navigator.userAgent.match(/iP[ha][od].*OS/)) {
		    cordova.plugins.notification.local.schedule({
			id: ++BroadwayiBeacon.notificationId,
			title: message,
			icon: "res://icon.png",
			smallIcon: "res://icon.png",
			data: page_url
		    });
		} else if(navigator.userAgent.match(/Android.*/)) {
			cordova.plugins.notification.local.schedule({
			id: ++BroadwayiBeacon.notificationId,
			title: "Broadway",
			text: message,
			icon: "res://icon.png",
			smallIcon: "res://icon.png",
			data: page_url
		    });
		}
	}

	cordova.plugins.locationManager.requestAlwaysAuthorization();
	this.delegate = new cordova.plugins.locationManager.Delegate();
	cordova.plugins.locationManager.setDelegate(this.delegate);

	this.delegate.didDetermineStateForRegion = function (result) {

	    console.log('[DOM] didDetermineStateForRegion: ' + JSON.stringify(result));

	    cordova.plugins.locationManager.appendToDeviceLog('[DOM] didDetermineStateForRegion: '
							      + JSON.stringify(result));
	    // Register when the phone enters the iBeacon area
	    
	    if(result.state == "CLRegionStateInside") {
	    	//startRanging();
	    }
	    
	    //BroadwayiBeacon.init();
	    startRanging();

	    //notificationGeneration('result.state: ' + result.state);

	};

	this.delegate.didStartMonitoringForRegion = function (pluginResult) {
	    console.log('didStartMonitoringForRegion:', pluginResult);


	    cordova.plugins.locationManager.appendToDeviceLog('[DOM] didStartMonitoringForRegion: '
							      + JSON.stringify(pluginResult));
	};

	this.delegate.didRangeBeaconsInRegion = function (pluginResult) {
	    if(!BroadwayiBeacon.repeat) {
		var beacon = pluginResult.beacons[0];
		var identifier = null;
		var message = null;
		console.log("Major " + beacon.major);
		console.log("Minor " + beacon.minor);
		for(i = 0; i < BroadwayiBeacon.regions.length; i++) {
		    if(beacon.major == BroadwayiBeacon.regions[i].major) {
			if(beacon.minor == BroadwayiBeacon.regions[i].minor) {
			    identifier = BroadwayiBeacon.regions[i].id;
			    // Loop out
			    i = BroadwayiBeacon.regions.length;
			}
		    }
		}
		message = BroadwayiBeacon.regionData[identifier];
		BroadwayiBeacon.latestMessage = message;
		page_url = BroadwayiBeacon.regionURL[identifier];
		BroadwayiBeacon.latestURL = page_url;

	      try{
	            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
			            dir.getFile("timelog.txt", {create:false}, function(fileEntry) {
				              BroadwayiBeacon.readFile(fileEntry);
			            });
		          });
	      } catch(exception) {
	          
	      }
		// Create the file to store the time if needed
		if(BroadwayiBeacon.repeatTime == null) {
			window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
				dir.getFile("timelog.txt", {create:true}, function(fileEntry) {
					BroadwayiBeacon.writeFile(fileEntry, null);
				});
			});
		}

		for(var i = 0; i < BroadwayiBeacon.repeatRegion.length; i++) {
			console.log(BroadwayiBeacon.repeatRegion[i]);
			if(identifier == BroadwayiBeacon.repeatRegion[i]) {
				console.log('Repeated');
				repeat = true;
				loopend = true;
			    i = BroadwayiBeacon.repeatRegion.length;
			} else {
				console.log('Not Repeated');
				repeat = false;
			}
		}

		if(!repeat) {
			//notificationGeneration(message, page_url);
			BroadwayiBeacon.repeatRegion.push(identifier);
		    BroadwayiBeacon.notificationId++;
		    // Update the timelog.txt whenever new iBeacon is checked. Time is not required to be updated.
		    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
			dir.getFile("timelog.txt", {create:true}, function(fileEntry) {
			    BroadwayiBeacon.writeFile(fileEntry, BroadwayiBeacon.repeatTime);
			});
		    });
		} else {
			receiveTime = new Date().getTime();
			timeDiff = receiveTime - BroadwayiBeacon.repeatTime;
			if(timeDiff >= BroadwayiBeacon.TIMEBOUND){
				window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
					dir.getFile("timelog.txt", {create:false}, function(fileEntry) {
					    BroadwayiBeacon.writeFile(fileEntry, null);
					    // Only repeat the message if the time is over 30 mins
					    //BroadwayiBeacon.notificationId++;
					    // Reset the array
					    BroadwayiBeacon.repeatRegion = [];
					});
				});
				notificationGeneration(message, page_url);
			}
		}
		// Check if the time is over the bound, 30 min = 1800000 milisec
		
	    }
	    
	    if(pluginResult.beacons.length == 0) {
			stopRanging();
	    }
	    console.log(pluginResult.beacons);
	};

	// Start monitoring.
	cordova.plugins.locationManager.startMonitoringForRegion(this.beaconRegion)
	    .fail(onError)
	    .done();
    },

    "stopMonitoring": function() {
    // Start monitoring.
	cordova.plugins.locationManager.stopMonitoringForRegion(BroadwayiBeacon.beaconRegion)
	    .fail(onError)
	    .done();
	cordova.plugins.locationManager.stopRangingBeaconsInRegion(BroadwayiBeacon.beaconRegion)
		.fail(onError)
		.done();
    }
}
    
