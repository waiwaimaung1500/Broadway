window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
                    
    hideContent();
    dir.getFile("log.txt", {create:false}, function(file) {
                            
        if(loginAuto == 'true') {
            //alert("a");
            tokenId = device.uuid;
            lang = $('input[name="language"]:checked', '#option').val();
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershipinformation", { transition: "none" } );
            switchSelectMenu('membership');
            memberId = window.localStorage.getItem("memberId");
            firstopen = false;
    		console.log(memberId);
    		memberInfo(memberId, lang);
            timeRefreshVar = setInterval(timeRefresh, 1000);
            loginCheckVar = setInterval(loginCheck, 10000);
    							
        } else {
            // No login not first time case
            window.localStorage.setItem('memberId', null);
            hideContent();
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "promotion");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/promotion.png");
            generatePromotion();
            switchSelectMenu('promotion');

            $(".main-content-promotion").css("display", "block");
            $("div#logout").css('display', 'none');
            firstopen = false;
        }

    	/* Function for right swipe */
    	$('.ui-mobile').on('swiperight', function(e) {
    		console.log("swipe");
    		$('#menuPanel').panel('open');
    	});
						
    });
    if(firstopen) {
        //alert("c");
        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#TAC", { transition: "none" } );
        
    }
});
				
// Reset iBeaconTempMessage after 30 mins to prevent keep receiving the messages.
setInterval(function() {
	iBeaconTempMessage = [];
}, 1800000);


app.initialize();
