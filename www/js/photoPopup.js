function showPopup(img, page) {
	photoPopUpOn = true;
	// FOR DEBUG USE
	//////////////////////////////
	login = false;
	//////////////////////////////
    overlayElement = document.createElement("div");
    overlayElement.className = 'popupOverlay';
    modalWindowElement = document.createElement("div");
    modalWindowElement.className = 'popupWindow';
	
	modalWindowTitle = document.createTextNode("TEST");
	modalWindowTitle.className = 'modalWindowTitle';
	modalWindowMsg = document.createTextNode("MESSAGE");
	modalWindowMsg = document.createElement("div");
	modalWindowMsg.className = 'modalWindowMsg';
    modalWindowMsg.innerHTML = "INNER";
	console.log("TEST");
	// Setting for width and height
	/* if(navigator.userAgent.match(/Android\s4\.[0123]/)){
	   modalWindowElement.style.width = "60%";
	   } else {
	   modalWindowElement.style.width = "60vw";
	   }
	   if(navigator.userAgent.match(/Android\s4\.[0123]/)){
	   modalWindowElement.style.marginTop = "1%";
	   } else {
	   modalWindowElement.style.marginTop = "1vh";
	   } */
	
	$('.ui-page-active').children().append(overlayElement);
	$('.ui-page-active').children().append(modalWindowElement);
	$('.popupWindow').append(
		$('<img>', {
			src: "img/cross.png",
			class: "crossImage",
		})
	);
	$('.popupWindow').append(
		$('<img>', {
			src: img,
			class: page + "-img",
			style: "width: 100%;"
		})
	)
		
		/* $(".member-promotion-img").panzoom(); */
	$("." + page + "-img").panzoom();
    setTimeout(function() {
        modalWindowElement.style.opacity = 1;
        overlayElement.style.opacity = 0.5;
        overlayElement.addEventListener("click", hidePopup, false);
		$('.crossImage').on("click", hidePopup);
		$(document).on("backbutton", hidePopup);
    }, 100);
}

//hide the modal overlay and popup window
function hidePopup() {
	photoPopUpOn = false;
    modalWindowElement.style.opacity = 0;
    overlayElement.style.opacity = 0;
    overlayElement.removeEventListener("click", hidePopup, false);
	/* generateMemberPromotion(); */
    setTimeout(function() {
		$('.popupOverlay').remove();
		$('.popupWindow').remove();
    }, 100);
}
