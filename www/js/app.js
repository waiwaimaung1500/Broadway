/*
* 	timeRefresh(): function in index.html
*	loginCheck(): function in index.html
*
*/

var BroadwayApp = {
	"app_loaded": false,
	"app_setting_loaded": $.Deferred(),
	"testing_on_desktop": true,
	"appVersion": null,
    "pageOpen": null,

	init: function() {
		console.log("[init]");

		_initSetting();
		
		if (document.URL.indexOf("http://") === -1) {
			BroadwayApp.testing_on_desktop = false;
		}
		
		$(document).ready(function () {
			console.log("jQuery finished loading");
			var deviceReadyDeferred = $.Deferred();
			var jqmReadyDeferred    = $.Deferred();

			if (BroadwayApp.testing_on_desktop) {
				console.log("Cordova finished loading");
				_onDeviceReady();
				deviceReadyDeferred.resolve();
			} else {
				document.addEventListener("deviceready", function () {
					console.log("Cordova finished loading in mobile");
					_onDeviceReady();
					deviceReadyDeferred.resolve();
				}, false);
			}
			
			$(document).one("pageinit", function () {
				console.log("jQuery.Mobile finished loading");
				jqmReadyDeferred.resolve();
			});
			
			$.when(deviceReadyDeferred, jqmReadyDeferred).then(function() {
				console.log("Cordova & jQuery.Mobile finished loading");
				_initPages();
				console.log("BroadwayApp finished loading");
				BroadwayApp.app_loaded = true;
			});
		});
		
		function _initSetting() {
			
		     (function($) {
		         $.fn.enterAsTab = function(options) {
		             var settings = $.extend({
		                 'allowSubmit': false
		             }, options);

		             $(this).find('input, select, textarea, button').on("keydown", {localSettings: settings}, function(event) {
		                 if (settings.allowSubmit) {
		                     var type = $(this).attr("type");
		                     if (type == "submit") {
		                         return true;
		                     }
		                 }
		                 // Handling the function "Enter" button to go to next field to input
		                 if (event.keyCode == 13) {
		                     console.log($(this).parents("form"));
		                     var inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
		                     var idx = inputs.index(this);
		                     if (idx == inputs.length - 1) {
		                         idx = -1;
		                     } else {
		                         inputs[idx + 1].focus(); // handles submit buttons
		                     }
		                     try {
		                         inputs[idx + 1].select();
		                     }
		                     catch (err) {
		                         // handle objects not offering select
		                     }
		                     return false;
		                 }
						 /* Function to block the back button to back the page.*/
		                 document.addEventListener("backbutton", function (e) {
		                     var inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
		                     try {
		                         inputs[0].select();
		                     }
		                     catch (err) {
		                         // handle objects not offering select
		                     }
		                     return false;
		                 }, false);
		             });
		             return this;
		         };
		     })(jQuery);
		}

		function _onDeviceReady() {
			console.log('deviceready');

			
			$(document).on('pageshow', '[data-role="page"]', function() {
		         loading('hide', 1000);
		    	 try{
		    	     pageID = $.mobile.activePage.attr('id');
		    	 }
		    	 catch (err) {
		    	     
		    	 }
		    	 if(pageID == 'productlist') {
		    	     console.log('productlist');
		    	 }
		    });

		    $("#login").enterAsTab({ 'allowSubmit': true});
		    $("#formregister").enterAsTab({ 'allowSubmit': true});
		    $(".menu-items").preventDoubleClick();
		    $("#app-btn-submit-register").preventDoubleClick();

			console.log('TESTING CODE LOADED')

			/* Callback for pause and resume action */
			document.addEventListener('pause', BroadwayApp.pauseChecking, false);
			document.addEventListener('resume', BroadwayApp.resumeChecking, false);

			/* Callback for back button action */
			document.addEventListener("backbutton", function(e) {
				_backButton();
			}, false);
			/*************************************/

			/* Callback for offline action */
			document.addEventListener("offline", function(e)  {
				$( ":mobile-pagecontainer" ).pagecontainer( "change", "#offline", { transition: "none" } );
				offline = true;
			}, false);
			/*************************************/

			/* Callback for online action */
			document.addEventListener("online", function(e)  {
				if(memberId == undefined)
					BroadwayApp.appSetting();
				if(offline) {
					$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
					if(loginToken == "true")
						membership();
					else
						promotion();
					offline = false;
				}
			}, false);
			/*************************************/

			// Callback to let the overlay become the only target for events
			$(document).on("pageshow", function(e, data) {
	            $('a#backbutton').css("pointer-events", "initial");
	            $('a#menupanel').css("pointer-events", "initial");
	        });

			// Callback to show the membership redemption item
			$(document).on("pageshow", "#membershippresentitem", function(e, data) {
	            console.log("ready");
    	    	$('.swiper-wrapper-point').slick({
                     infinite: false,
                     slideToShow: 2,
                     slideToScroll: 2,
                     centerPadding: '30px',
                     centerMode: true
                     
    	    	});
            	showGift(initialRedemptionEvent, initialRedemptionPoint);
            	switchPoint(initialRedemptionPoint);
         	});

			// Callback to open the menu panel
			$('img#menupanelbtn').on("click", function() {
				$('#menuPanel').panel('open');
				if(photoPopUpOn) {
					hidePopup();
				}
			});

			$('img#back').on("click", function() {
				window.history.back();
				if(pageID == "membershippresentitem")
					generateRedmption();
				$('a#backbutton').css("pointer-events", "none");
			});

			// Callback to open the option overlay
			$('img#optionbtn').on("click", function() {
				menuOn = true;
				$('.modalOverlay').css('visibility', 'visible');
				$('.modalWindow').css('visibility', 'visible');
			});

			// Callback to close the overlay when there is click on the overlay(.modalOverlay)
			// Callback to close the overlay when the close button is clicked (img#optioncolourbtn)
			$('.modalOverlay, img#optioncolourbtn').on("click", function() {
		        $('.modalOverlay').css('visibility', 'hidden');
		        $('.modalWindow').css('visibility', 'hidden');
		    });

		    // Callback to perform change language
		    $('#option input').on("change", languageChange);

			// Show the statusbar, should be normal when setting in config.xml. Backup use.
			StatusBar.overlaysWebView(false);

            $('#tutorial1').attr('src', 'img/tutorial1.png');
            $('#tutorial2').attr('src', 'img/tutorial2.png');
            $('#tutorial3').attr('src', 'img/tutorial3.png');

		    /*navigator.globalization.getPreferredLanguage(
		       function (language) {alert('language: ' + language.value + '\n');},
		       function () {alert('Error getting language\n');}
		       );*/

			cordova.plugins.notification.local.on("click", function (notification) {
			    data = notification.data;
			    BroadwayApp.pageOpen = data;

			    switch(data) {
			    case 'promotions':
				setTimeout(promotion(), 3000);
				break;
			    case 'member-promotions':
				setTimeout(membershipdiscount(), 3000);
				break;
			    case 'updatednews':
				setTimeout(updatednews(), 3000);
				break;
			    case 'hotproducts':
				setTimeout(hotproducts(), 3000);
				break;
			    case 'membershippresent':
				setTimeout(membershippresent(), 3000);
				break;
			    case 'locations':
				setTimeout(locations(), 3000);
				break;
			    }
			});

			/* Part to decide the language */
			lang = window.localStorage.getItem('lang');
	        if(lang == null) {
	            window.localStorage.setItem('lang', 'zh-hant');
	            lang = 'zh-hant';
	        }
	        $('input[name="language"][value=' + lang + ']').attr("checked", "true");
	        html10n.bind("localized", function() {
	            if($('.selected-area')[0] != null) {
	                $('.selected-area')[0].click();
	            }
	        });

	        languageChange();
	        /*************************************/

			/* Fix for iPhone zoom problem */
			if (/iPhone/.test(navigator.userAgent) && !window.MSStream) {
				$(document).on("focus", "input, textarea, select", function() {
					$('meta[name=viewport]').remove();
					$('head').append('<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />');
				});
				
				$(document).on("blur", "input, textarea, select", function() {
					$('meta[name=viewport]').remove();
					$('head').append('<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=4, minimum-scale=1, width=device-width" />');
				});
			}
			/*************************************/
			
			try{
				tokenId = device.uuid;
				cordova.plugins.backgroundMode.enable();
				
				cordova.plugins.backgroundMode.setDefaults({
					silent: true
				});
				//html10n.localize(lang);
				
				cordova.plugins.backgroundMode.configure({
					silent: true
				});
				
			} catch(e) {
				
			}
            // Load the app setting when the device is ready
            BroadwayApp.appSetting();

			// Adjust the image button tapping behaviour according to the Android version detected.
            $('img#app-btn-register').on('tap', function() {
				if(navigator.userAgent.match(/Android\s4\.[0123]/)) {
					$('a#register').click();
				} else {
					$('a#register')[0].click();
				}
            });
            
            
            // Reset iBeaconTempMessage after 30 mins to prevent keep receiving the messages.
            setInterval(function() {
                iBeaconTempMessage = [];
            }, 1800000);
			pageChangevar = setInterval(pageChange, 1000);
			//closeSplashVar = setInterval(closeSplash, 1000);
            
		    // Set the notification service depends on the version.
		    
            /*
            * Whether there is a toggle between censorship to enable which service depends on the testing result and requirement.
            * 4/6/2016
            */
            // Using jPush plugin for notification in China. Should be for Android only.
            if(!censorship) {
				app.initialize();
		    } else {
			jPush.init();
			console.log('jPush');
				document.addEventListener("jpush.receiveNotification", jPush.onReceiveNotification, false);
				document.addEventListener("jpush.openNotification", jPush.onOpenNotification, false);
				document.addEventListener("jpush.receiveMessage", jPush.onReceiveMessage, false);
		    }
		};

		function _initPages() {
			console.log("[initPages]");
			$(document).bind("pageinit", _init);
			
			function _init () {
				/* Part to prevent the problem when closing the menu panel */
				$("[data-role=panel] a").on("click", function () {
					if($(this).attr("href") == "#"+$.mobile.activePage[0].id) {
						$("[data-role=panel]").panel("close");
					}
				});
				/*************************************/
			};
		};

		function _backButton() {
			function exitBroadway(buttonIndex) {
				if(buttonIndex == 1)
					navigator.app.exitApp();
			}

			// Check the page to decide the action
			var pageID;
			try{
				pageID = $.mobile.activePage.attr('id');
			}
			catch (err) {
				
			}
			if(pageID == 'general') {
				if(!photoPopUpOn) {
					navigator.notification.confirm(
						window.html10n.get("exitMessage"),
						exitBroadway,
						window.html10n.get("exitTitle"),
						[window.html10n.get("confirm"), window.html10n.get("cancel")]);
				}
			} else {
				if(!photoPopUpOn)
					navigator.app.backHistory();
			}
			// Two history for having T&C and tutorial which only show in the first time of using the app
		};
	},

	"utilities": {
	},

	initialilzeMap: function() {
        var mapProp = {
            center: new google.maps.LatLng(0, 0),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map($("#googleMap")[0], mapProp);
	},

	appSetting: function() {

		mAppInBackground = false;
		mAppInForeground = true;
		
	    function linkToStore(buttonIndex) {
			if(buttonIndex == 1) {
				if(navigator.userAgent.match(/Android.*|/)) {
				    window.open('market://details?id=hk.com.broadway', '_system');
				}
			}
	    }
	    

	    /* Register the tap event for login */
        $('img#app-btn-login').on('tap', function() {
            loginAction();
        });

        /* Google map loading */
		if(!censorship)
			google.maps.event.addDomListener(window, 'load', BroadwayApp.initialilzeMap);
		
		/*************************************/

        var settings = {
            "async": true,
            "url": APIaddress,
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            },
            "processData": false,
            "data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
        }

        $.ajax(settings).done(function (data) {
            /* Check if the user is first time use the app or not */
            var firstopen = true;
            /* Check if the user is autologin or not */
            loginToken = window.localStorage.getItem('loginToken');
            loginAuto = window.localStorage.getItem('loginAuto');

            $('.main-content-updatednews').empty();
            try {
                data = JSON.parse(data);
            } catch(e) {
                console.log('JSON already');
            }
	    	console.log(data.ios);
            if(navigator.userAgent.match(/iP[ha][od].*OS/)) {
            	BroadwayApp.appVersion = data.ios;
			} else if(navigator.userAgent.match(/Android.*/)) {
				if(censorship)
					BroadwayApp.appVersion = data.baidu;
				else
					BroadwayApp.appVersion = data.android;
			}
			/*
            cordova.getAppVersion.getVersionNumber(function (version) {
		console.log(version != BroadwayApp.appVersion);
		if(version != BroadwayApp.appVersion) {
		    navigator.notification.confirm(
			window.html10n.get('updateMessage'),
			linkToStore,
			window.html10n.get('updateTitle'),
			[window.html10n.get('confirm'), window.html10n.get('cancel')]
		    );
		}
	    });*/

            console.log("AppData");
            console.log(data.links_setting[13]);
            forgetPasswordURL = data.links_setting[0].forgot_password;
            registerAPI = data.links_setting[1].registerAPI;
            memberAPI = data.links_setting[2].memberAPI;
            loginAPI = data.links_setting[3].loginAPI;
            logoutAPI = data.links_setting[4].logoutAPI;
            redemptionsAPI = data.links_setting[5].redemptionsAPI;
            memberPromotionsAPI = data.links_setting[6].memberPromotionsAPI;
            shopsAPI = data.links_setting[7].shopsAPI;
            shopAPI = data.links_setting[8].shopAPI;
            promotionAPI = data.links_setting[9].promotionAPI;
            categoriesAPI = data.links_setting[10].categoriesAPI;
            productsAPI = data.links_setting[11].productsAPI;
            productAPI = data.links_setting[12].productAPI;
            if(settings.url == APIaddress) {
            	beaconListAPI = data.links_setting[13].beaconListAPI;
            	beaconMsgAPI = data.links_setting[14].beaconMsgAPI;
            } else {
            	beaconListAPI = data.links_setting[14].beaconListAPI;
            	beaconMsgAPI = data.links_setting[13].beaconMsgAPI;	
            }
            
            $('#animation').attr('src', data.app_setting.animation_location);
            if(data.news_setting.length != 0) {
                $('li.updatednews').attr("style", "display: initial");
                for(i = 0; i < data.news_setting.length; i++) {
                    $('<img>', {
                        src: data.news_setting[i].image_url,
                        class: "promotionitem"
                        
                    }).appendTo('.main-content-updatednews');
                }
            } else {
                $('li.updatednews').attr("style", "display: none");
            }

			/* Callback to load iBeacon information */
			BroadwayApp.app_setting_loaded.resolve()
					      .done(function() {
                    
                    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
                        
                        hideContent();
                        dir.getFile("log.txt", {create:false}, function(file) {
                            if(loginAuto == 'true') {
                                //alert("a");
                                tokenId = device.uuid;
                                lang = $('input[name="language"]:checked', '#option').val();
                                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
                                $( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershipinformation", { transition: "none" } );
                                switchSelectMenu('membership');
                                memberId = window.localStorage.getItem("memberId");
                                firstopen = false;
						        membership();
                                console.log(memberId);
                                //memberInfo(memberId, lang);
                                timeRefreshVar = setInterval(timeRefresh, 1000);
                                loginCheckVar = setInterval(loginCheck, 10000);
                                BroadwayApp.specificPage();
                                
                            } else {
                                // No login not first time case
                                //alert("b");
                                // Set loginToken back to false to prevent member checking continuously.
						                    loginToken = false;
						                    window.localStorage.setItem("loginToken", false);
                                window.localStorage.setItem('memberId', null);

                                hideContent();
						                    
			                    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
			                    $(".ui-panel-open").panel("close");
			                    $(".ui-page-active").find(".header-title").attr("data-l10n-id", "promotion");
			                    $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/promotion.png");
			                    // generatePromotion();
			                    switchSelectMenu('promotion');
                                // Receive the iBeacon or push message and jump to specific page
                                BroadwayApp.specificPage();
						                    
                                $("div#logout").css('display', 'none');
                                firstopen = false;
                            }

			                //$("div#TAC").empty();
			                $("a").remove("#app-btn-accept");

			                // Cross image usage
			                $(".crossImageTAC").css("display", 'initial');
			                $(".crossImageTAC").on("click", function() {
			            	    $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
			                });

                            /* Function for right swipe */
                            $('.ui-mobile').on('swiperight', function(e) {
			                    var pageID;
			                    try{
				                      pageID = $.mobile.activePage.attr('id');
			                    } catch (err) {
				                      
			                    }
			                    
			                    if(!offline) {
				                    if(pageID =="general") {
				                    	console.log("swipe");
					                    $('#menuPanel').panel('open');
					                    if(photoPopUpOn) {
					                    	hidePopup();
					                    }
				                        if(menuOn) {
					                          $('.modalOverlay').css('visibility', 'hidden');
					                          $('.modalWindow').css('visibility', 'hidden');
				                        }
				                    }
                                }
		        	        });
                            /*************************************/

                            
                        });
                        if(firstopen) {
                            //alert("c");
                            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#TAC", { transition: "none" } );
                            
                        }
                        BroadwayApp.iBeaconLoad();
                    });
                });
        });
	},

    specificPage: function() {
        try{
	    if(typeof(BroadwayApp.pageOpen) != 'undefined') {
		hideContent();
                switch(BroadwayApp.pageOpen) {
                case 'promotions':
					          setTimeout(promotion(), 3000);
                    break;
                case 'member-promotions':
					          setTimeout(membershipdiscount(), 3000);
                    break;
                case 'updatednews':
					          setTimeout(updatednews(), 3000);
                    break;
                case 'hotproducts':
					          setTimeout(hotproducts(), 3000);
                    break;
                case 'membershippresent':
					          setTimeout(membershippresent(), 3000);
                    break;
                case 'locations':
					          setTimeout(locations(), 3000);
                    break;
		default:
			if(loginAuto == 'true')
				setTimeout(membership(), 3000);
			else
			    setTimeout(promotion(), 3000);
		    break;
                }
	    }
            else if(typeof(jPush.notification.extras.pageOpen) != 'undefined' ||
	      jPush.notification != 'undefined') {
                hideContent();
                switch(pageOpen) {
                case 'promotions':
					          setTimeout(promotion(), 3000);
                    break;
                case 'member-promotions':
					          setTimeout(membershipdiscount(), 3000);
                    break;
                case 'updatednews':
					          setTimeout(updatednews(), 3000);
                    break;
                case 'hotproducts':
					          setTimeout(hotproducts(), 3000);
                    break;
                case 'membershippresent':
					          setTimeout(membershippresent(), 3000);
                    break;
                case 'locations':
					          setTimeout(locations(), 3000);
                    break;

		default:
			if(loginAuto == 'true')
				setTimeout(membership(), 3000);
			else
			    setTimeout(promotion(), 3000);
		    break;
                }
            }
        } catch (exception) {
            promotion();
        }
    },

	iBeaconLoad: function() {
        var iBeaconListSetting = {
            "async": true,
            "url": beaconListAPI,
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            },
            "processData": false,
            "data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
        }
        var iBeaconInfoSetting = {
            "async": true,
            "url": beaconMsgAPI,
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            },
            "processData": false,
            "data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
        }

        $.ajax(iBeaconListSetting).done(function(data) {
            try {
                data = JSON.parse(data);
            } catch(e) {
                console.log('JSON already');
            }
            //data = JSON.parse(data);
            g_mRegions = data.regions;
            $.ajax(iBeaconInfoSetting).done(function(data) {
                try {
                    data = JSON.parse(data);
                } catch(e) {
                    console.log('JSON already');
                }
                //data = JSON.parse(data);
                g_mRegionData = data.messages;
                g_mRegionURL = data.page_url;

		console.log(g_mRegionURL);

		BroadwayiBeacon.init();
                // broadwayiBeaconSetting();
                // broadwayiBeacon.initialize();
            })
        });
	},

	pauseChecking: function() {
        console.log('pauseChecking');
        loginToken = 'false';
		_onAppToBackground();
        console.log(loginToken);

		function _onAppToBackground() {
			mAppInBackground = true;
			mAppInForeground = false;
			var reg = new RegExp(/Android.*/);
			console.log('mAppInBackground: ' + mAppInBackground);
			if(reg.test(navigator.userAgent)) {
				BroadwayiBeacon.startMonitoring();
			}
			// 5seconds to quit app
			// Time = 5 x 60 x 1000
			quitApp = setTimeout(function() {
				navigator.app.exitApp();
			}, 300000);
			//stopNearestBeaconDisplayTimer();
			//registerPushwooshIOS();
		    // Stop monitoring for background
		    /* BroadwayiBeacon.stopMonitoring();*/
		    // Special case differs from notificationGeneration in iBeacons/appiOS.js
		    // Generate the latest message
		    if(BroadwayiBeacon.latestMessage != null &&
		      BroadwayiBeacon.notificationId != BroadwayiBeacon.latestId) {
		      	/*
				cordova.plugins.notification.local.schedule({
				    id: BroadwayiBeacon.notificationId,
				    title: BroadwayiBeacon.latestMessage,
				    icon: "res://icon.png",
				    smallIcon: "res://icon.png",
				    data: BroadwayiBeacon.latestURL
				});
				*/
				if(navigator.userAgent.match(/iP[ha][od].*OS/)) {
				    cordova.plugins.notification.local.schedule({
					id: BroadwayiBeacon.notificationId,
					title: BroadwayiBeacon.latestMessage,
					icon: "res://icon.png",
					smallIcon: "res://icon.png",
					data: BroadwayiBeacon.latestURL
				    });
				} else if(navigator.userAgent.match(/Android.*/)) {
					cordova.plugins.notification.local.schedule({
					id: BroadwayiBeacon.notificationId,
					title: "Broadway",
					text: BroadwayiBeacon.latestMessage,
					icon: "res://icon.png",
					smallIcon: "res://icon.png",
					data: BroadwayiBeacon.latestURL
				    });
				}
				BroadwayiBeacon.latestId = BroadwayiBeacon.notificationId;
		    }
		}
	},

	resumeChecking: function() {
        console.log('resumeChecking');
 		var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
        pushNotification.setApplicationIconBadgeNumber(0);
        loginToken = window.localStorage.getItem('loginToken');
        console.log(loginToken);
		_onAppToForeground();
        if(loginToken == "true") {
			memberInfo(memberId, lang);
			languageChange();
			mAppInBackground = false;
		}

		
		function _onAppToForeground() {
			console.log('mAppInBackground: ' + mAppInBackground);
			mAppInBackground = false;
			mAppInForeground = true;
			if(navigator.userAgent.match(/Android.*/)) {
				BroadwayiBeacon.stopMonitoring();
			}
			//startNearestBeaconDisplayTimer();
			//deregister();
			clearTimeout(quitApp);
			setTimeout(function() {
				//languageChange_member();
				
			}, 2000);
		    // Start monitoring for background
		    BroadwayiBeacon.startMonitoring();
		}
	},
};
