/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
function deregister () {
    var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
    
    //set push notification callback before we initialize the plugin
    pushNotification.unregisterDevice(
        function (token) {
            console.log("unregisterDevice, token: " + token);
            //alert("unregisterDevice, token: " + token);
        },
        
        function (status) {
            console.warn("registerDevice failed, status:" + status);
            //alert("registerDevice failed, status:" + status);
        }
    );
}
function registerPushwooshIOS() {
 	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");

 	//set push notification callback before we initialize the plugin
	document.addEventListener('push-notification',
							  function(event)
		{
			//get the notification payload
			var notification = event.notification;
			/* var obj = JSON.parse(notification.aps.u); */

			//display alert to the user for example
			/* alert(notification.u.link); */
            console.log(notification);
            console.log($(':mobile-pagecontainer').pagecontainer('getActivePage'));
            var onStart = notification.onStart;
            console.log(onStart);
            //alert(notification.aps.alert);
            //notification = JSON.parse(notification);
            var pageOpen = Object.keys(notification.userdata.page[0])[0];
            console.log(notification);
            //var anchorId = notification.u.page[0].promotions;
            
            console.log(pageOpen);
            //console.log(anchorId);
            
            if(pageOpen == "promotions") {
                if(onStart)
                    pageToChange = pageOpen;
                if(mAppInBackground)
                    setTimeout(promotion(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            
            if(pageOpen == "member-promotions") {
                if(onStart)
                    pageToChange = pageOpen;
                if(mAppInBackground)
                    setTimeout(membershipdiscount(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            
            if(pageOpen == "updatednews") {
                if(onStart)
                    pageToChange = pageOpen;
                if(mAppInBackground)
                    setTimeout(updatednews(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            if(pageOpen == "hotproducts") {
                if(onStart)
                    pageToChange = pageOpen;
                if(mAppInBackground)
                    setTimeout(hotproducts(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            if(pageOpen == "membershippresent") {
                if(onStart)
                    pageToChange = pageOpen;
                if(mAppInBackground)
                    setTimeout(membershippresent(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            if(pageOpen == "locations") {
                if(onStart) {
                    pageToChange = pageOpen;
                }
                if(mAppInBackground)
                    setTimeout(locations(), 1000);
                //locations();
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            
            
			//to view full push payload
			/* alert(JSON.stringify(notification)); */
			
			//clear the app badge
			pushNotification.setApplicationIconBadgeNumber(0);
            pushNotification.startLocationTracking(
                function(success) {
                    //alert(success);
                    //alert("Success");
                },
                function(fail) {
                    //alert("fail");
                });
		}
	);

	//initialize the plugin
    pushNotification.onDeviceReady({pw_appid:"4DB09-5DAFF"});

	//register for pushes
	pushNotification.registerDevice(
		function(status)
		{
			var deviceToken = status['deviceToken'];
			console.warn('registerDevice: ' + deviceToken);
			onPushwooshiOSInitialized(deviceToken);
		},
		function(status)
		{
			console.warn('failed to register : ' + JSON.stringify(status));
			//alert(JSON.stringify(['failed to register ', status]));
		}
	);
	
	//reset badges on start
	pushNotification.setApplicationIconBadgeNumber(0);
    
    pushNotification.startBeaconPushes(function() {
        console.log('success');
    },
                                       function() {
                                           console.log('fail');
                                       }
    );
}

function onPushwooshiOSInitialized(pushToken)
{
	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
	//retrieve the tags for the device
	pushNotification.getTags(
		function(tags) {
			console.warn('tags for the device: ' + JSON.stringify(tags));
		},
		function(error) {
			console.warn('get tags error: ' + JSON.stringify(error));
		}
	);

	//example how to get push token at a later time 
	pushNotification.getPushToken(
		function(token)
		{
			console.warn('push token device: ' + token);
		}
	);

	//example how to get Pushwoosh HWID to communicate with Pushwoosh API
	pushNotification.getPushwooshHWID(
		function(token) {
			console.warn('Pushwoosh HWID: ' + token);
		}
	);

	//start geo tracking.
    pushNotification.startLocationTracking();
    /*pushNotification.startLocationTracking(
       function(success) {
       alert(success);
       alert("Success");
       },
       function(fail) {
       alert("fail");
       });
       */
    
}
