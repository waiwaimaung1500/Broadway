var id = 0;

function registerPushwooshAndroid() {
 	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");

	// Check if there is token in the app.
	pushNotification.getPushToken(
		function(token)
		{
			console.warn('push token: ' + token);
			//onPushwooshAndroidRegistration();
			if(token == "")
				onPushwooshAndroidRegistration();
		},
		function() {
			onPushwooshAndroidRegistration();
		}
	);
}

function onPushwooshAndroidRegistration() {
 	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
 	console.warn('Registering...');
	//initialize Pushwoosh with projectid: "GOOGLE_PROJECT_ID", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
	// Release usage
	
	pushNotification.onDeviceReady({ 
		projectid: "940908672689", 
		pw_appid : "4DB09-5DAFF",
		serviceName: ""
	});

	// Internal test, use personal google cloud service
	
	/* pushNotification.onDeviceReady({ 
	   projectid: "851108781592",
	   appid: "4CEE2-34FFC",
	   serviceName: "" 
	   });*/
	
	
	pushNotification.setMultiNotificationMode();
	
	pushNotification.registerDevice(
		function(token)
		{
			console.log('token registered: ' + token);
			//callback when pushwoosh is ready
			pushWooshToken = token;
			pushNotification.startLocationTracking(
			function(token)
			{
				console.log('location registered: ' + token);
				//callback when pushwoosh is ready
			},
			function(status)
			{
				alert("failed to register: " +  status);
				console.warn(JSON.stringify(['failed to register ', status]));
			}
			);
		},
		function(status)
		{
			alert("failed to register: " +  status);
		    console.warn(JSON.stringify(['failed to register ', status]));
		}
	);
	pushNotification.setApplicationIconBadgeNumber(0);
	/*
	pushNotification.startLocationTracking(
			function(token)
			{
				console.log('location registered: ' + token);
				//callback when pushwoosh is ready
			},
			function(status)
			{
				alert("failed to register: " +  status);
				console.warn(JSON.stringify(['failed to register ', status]));
			}
			);
			*/
	
		
}
// Check if the open of the app involves the data from notification
function listenPushwooshAndroidOnDeviceReady() {
 	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
 	/*
	pushNotification.getLaunchNotification(
		function(notification) {
			if(notification != null) {
				var string = JSON.stringify(notification);
				var obj = JSON.parse(string);
				console.log(obj.title);
				console.log(obj.userdata.link);
				if(obj.userdata.link == "bank")
					location.assign("#second");
			} else {
				console.log("NULL");
			}
		}
	);
	//reset badges on start
	//pushNotification.setApplicationIconBadgeNumber(0);
	/*pushNotification.startLocationTracking(
		function(token)
		{
			console.log('location registered: ' + token);
			//callback when pushwoosh is ready
		},
		function(status)
		{
			alert("failed to register: " +  status);
		    console.warn(JSON.stringify(['failed to register ', status]));
		}
	);*/
	document.addEventListener("push-notification",
		function(event) {
			console.log("notification received");

			console.log(event);
			//get the notification payload
			var notification = event.notification;
			console.log(notification);
			// Check if the notfication get is from background to foreground
			var onStart = notification.onStart;
			var foreground = notification.foreground;
			//display alert to the user for example
			//alert(notification);
			
			try {
				/* var obj = JSON.parse(notification.userdata);*/
				var obj = notification.userdata;
				console.log(obj);
				console.log(obj.page);
				console.log(obj.page[0]);
				console.log($(':mobile-pagecontainer').pagecontainer('getActivePage'));
				//alert(notification.aps.alert);
				//notification = JSON.parse(notification);
				var pageOpen = Object.keys(obj.page[0])[0];
				var anchorId = obj.page[0].promotions;
				
				console.log(pageOpen);
				console.log(anchorId);
			} catch(err) {
				console.warn(err + ": No u field");
			}
			
			if(foreground) {
				
				var message = notification.message;
				var header = notification.android.header;
				cordova.plugins.notification.local.schedule({
					id: ++id,
					title: header,
					text: message,
					icon: "../../img/icon.png"
				});
				/*
				pushNotification.createLocalNotification(
					{msg: "TEST", seconds: 10, userData:"test"},
					function() {
						console.warn("Local GOOD");
					},
					function() {
						console.warn("FAIL");
					}
			   );
			   */
			}
            
            if(pageOpen == "promotions") {
				if(onStart)
					pageToChange = pageOpen;
				if(mAppInBackground)
					setTimeout(promotion(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            
            if(pageOpen == "member-promotions") {
				if(onStart)
					pageToChange = pageOpen;
				if(mAppInBackground)
					setTimeout(membershipdiscount(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
            
            if(pageOpen == "updatednews") {
				if(onStart)
					pageToChange = pageOpen;
				if(mAppInBackground)
					setTimeout(updatednews(), 1000);
                //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
            }
			if(pageOpen == "hotproducts") {
				if(onStart)
					pageToChange = pageOpen;
				if(mAppInBackground)
					setTimeout(hotproducts(), 1000);
                              //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
			}
			if(pageOpen == "membershippresent") {
				if(onStart)
					pageToChange = pageOpen;
				if(mAppInBackground)
					setTimeout(membershippresent(), 1000);
                              //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
			}
			if(pageOpen == "locations") {
				if(onStart) {
					pageToChange = pageOpen;
				}
				if(mAppInBackground)
					setTimeout(locations(), 1000);
			  //locations();
			  //setTimeout($.mobile.silentScroll($("'#item" + anchorId + "'").offset().top), 500);
			}
			
			pushNotification.startLocationTracking(
				function(token) {
					console.log('location registered: ' + token);
					//callback when pushwoosh is ready
				},
				function(status) {
					//alert("failed to register: " +  status);
					console.warn(JSON.stringify(['failed to register ', status]));
				}
			)
		}
	);
}

function listenPushwooshAndroid() {
 	var pushNotification = cordova.require("pushwoosh-cordova-plugin.PushNotification");
	pushNotification.getLaunchNotification(
		function(notification) {
			if(notification != null) {
				console.log(JSON.stringify(notification));
			} else {
				console.log("NULL");
			}
		}
	);

	//set push notifications handler
	document.addEventListener('push-notification',
		function(event)
		{
/*
            var title = event.notification.title;
            var userData = event.notification.userdata;
            var additionalData = event.notification.u;
			var obj = JSON.parse(additionalData);
*/
			/* pushNotification.createLocalNotification(
			   {msg: "TEST", seconds: 10, userData:"test"},
			   function() {
			   console.warn("Local GOOD");
			   },
			   function() {
			   console.warn("FAIL");
			   }
			   ); */

				/* pushNotification.setBeaconBackgroundMode(
				   function() {
				   console.warn('BeaconBackgoundMode');
				   },
				   function() {
				   console.warn('Normal');
				   }
				   ); */
/*
			console.log(event);
			
            //dump custom data to the console if it exists
            if(typeof(userData) != "undefined") {
				console.warn('user data: ' + JSON.stringify(userData));
			}
			
			//and show alert
			if(obj.link == "bank") {
				location.assign("#second");
			}
*/
			//stopping geopushes
			//pushNotification.stopGeoPushes();
		}
	);
}
