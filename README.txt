Instruction:

*For the instruction “Run the following:”, execute the command in terminal, where the home folder should be the same folder as this README.txt file.
First time in getting the source (The folder WITHOUT platforms/ folder)
Run the following:
./init.sh

To build the Android version, 
Run the following:
./make_android.sh

If you wish to run the program directly on the connected Android device,
Run the following:
./make_android.sh run

To build the iOS version,
Run the following:
./make_ios.sh

To change the version code if necessary,
Run the following:
./version.sh (VERSION)

(VERSION) represents the new version number for the application. 

Checkpoint
Make sure the Android 6.0 (API 23) SDK Platform is installed.
Check censorship variable to ensure which version is built. Android only